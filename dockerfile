FROM registry.gitlab.com/erickstorck/kairos/dependencies:rasp AS build
COPY . .
RUN yarn build

FROM nginx:alpine AS release
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /build/kairos/dist/kairos-panel /usr/share/nginx/html
CMD ["/bin/sh",  "-c",  "envsubst < /usr/share/nginx/html/assets/env.template.js > /usr/share/nginx/html/assets/env.js && exec nginx -g 'daemon off;'"]
EXPOSE 80
