(function (window) {
    window.env = window.env || {};

    // Environment variables
    window["apiUrl"] = "${API_URL}";
})(this);