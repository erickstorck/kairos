import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {Router} from '@angular/router';
import { AuthService } from "./auth.service";

@Injectable()

export class AuthInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService, private router: Router) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>>  {
        const authToken = this.authService.getToken();
        req = req.clone({
            setHeaders: {
                Authorization: "Bearer " + authToken,
                'Access-Control-Allow-Origin': "*"
            }
        });
        return next
            .handle(req)
            .pipe(
                catchError( error  => {
                    if (error.status == 401) {
                        this.authService.doLogout();
                    } else {
                      console.log(error['error'])
                    }
                    return throwError(error);
                }));
   }
}
