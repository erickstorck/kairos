export class Project {
    description: String;
    id: String;
    name: String;
    features: Array<String>;
    length: Number;
}
