import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { decodeToken } from './utils/token';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return this.checkPermission(next);
  }

  checkPermission(route: any) {
    const token = decodeToken(sessionStorage.getItem('access_token'));
    let hasPermission = false;

    if (token['permission'].includes(route.data.role)) {
      hasPermission = true
    }

    if (token['permission'].includes('admin')) {
      hasPermission = true
    }

    if (!hasPermission) {
      alert('Sem Autorização')
    }

    return hasPermission;
  }

}
