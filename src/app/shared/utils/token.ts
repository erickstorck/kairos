import jwtDecode, { JwtPayload } from "jwt-decode";

export function decodeToken(token): any {
  const decoded = jwtDecode<JwtPayload>(token);
  return decoded;
}
