export function parseCurrentPage(currentPage: string){
    currentPage = currentPage.replace(/\//g,'');
    let splitedString: string[] = currentPage.split('-')
    splitedString = splitedString.map((item)=>{
        return item.charAt(0).toUpperCase() + item.slice(1);
    });
    currentPage = splitedString.join(" ");
    return currentPage;
}

export function parseTableColumn(cloumn: string){
    cloumn = cloumn.replace(/-/g,'_');
    let splitedString: string[] = cloumn.split('_');
    splitedString = splitedString.map((item)=>{
        return item.charAt(0).toUpperCase() + item.slice(1);
    });
    cloumn = splitedString.join(" ");
    return cloumn;

}
