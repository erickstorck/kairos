import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private loadingSubject = new Subject<boolean>();

  constructor() { }


  startingLoading() {
    this.loadingSubject.next(true);
  }

  endingLoading() {
    this.loadingSubject.next(false);
  }

  loadingListener() {
    return this.loadingSubject.asObservable();
  }
}
