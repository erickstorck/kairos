import { Injectable } from '@angular/core';
import { User } from './models/user';
import { catchError, mergeMap, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { decodeToken } from './utils/token';

import { environment } from './../../environments/environment';
import { decode } from 'querystring';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  headers = new HttpHeaders().set('Content-Type', 'text/plain');
  currentUser = {};
  constructor(
    private http: HttpClient,
    public router: Router
  ) {
  }

  private clearStorage() {
    sessionStorage.removeItem('access_token');
    sessionStorage.removeItem('featuresParsed');
    sessionStorage.removeItem('feature-selected');
    sessionStorage.removeItem('user-name');
    sessionStorage.removeItem('project');
  }

  checkToken() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set('Access-Control-Allow-Credentials', 'true')
      .set('Access-Control-Allow-Origin', '*')
    const token = sessionStorage.getItem('access_token');
    return this.http.post(`${environment.apiUrl}/token/check`, { token }, { headers, responseType: "text" })
      .pipe(
        mergeMap(res => res),
        catchError((err) => {
          this.clearStorage();
          alert("Token inválido.\nVocê será redirecionado para fazer login");
          this.router.navigate(['login']);
          return throwError(() => new Error(err));
        })
      );
  }

  resetUserPassword(email: string) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set('Access-Control-Allow-Credentials', 'true')
      .set('Access-Control-Allow-Origin', '*')
    return this.http.post(`${environment.apiUrl}/user/reset_pwd`, { email: email }, { headers, responseType: 'text' });
  }

  signIn(user: any) {
    this.clearStorage();
    const headers = new HttpHeaders()
      .set('Access-Control-Allow-Credentials', 'true')
      .set('Access-Control-Allow-Origin', '*')
    return this.http.post<any>(`${environment.apiUrl}/login`, user, { headers });
  }

  getByEmail(email: string) {
    return this.http.post<any[]>(`${environment.apiUrl}/user`, { email: email });
  }

  getUsers() {
    return this.http.post<any[]>(`${environment.apiUrl}/users`, {});
  }

  createUser(user) {
    return this.http.post(`${environment.apiUrl}/user/create`, user, { responseType: 'json' });
  }

  updateUser(user) {
    return this.http.post(`${environment.apiUrl}/user/update`, user, { responseType: 'text' });
  }

  getToken() {
    return sessionStorage.getItem('access_token');
  }

  doLogout() {
    let token = decodeToken(sessionStorage.getItem('access_token'))
    if (token != null) {
      return this.http.post<any>(`${environment.apiUrl}/logout`, { email: token['email'] })
        .subscribe((res: any) => {
          this.clearStorage();
          this.router.navigate(['login']);
        },
          (error: any) => {
            this.clearStorage();
            this.router.navigate(['login']);
          });
    }
  }

  deleteUser(id) {
    return this.http.delete(`${environment.apiUrl}/user/${id}`, { responseType: 'text' })
  }
}
