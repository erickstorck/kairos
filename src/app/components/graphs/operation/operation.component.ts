import { Component, OnInit } from '@angular/core';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { SiloService } from '../../silos/silo.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as d3 from 'd3';
import * as dc from 'dc';
import * as crossfilter from 'crossfilter2/crossfilter';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-operation',
  templateUrl: './operation.component.html',
  styleUrls: ['./operation.component.css']
})
export class OperationComponent implements OnInit {

  group_name: string = "chartContainer";
  minDate: Date;
  maxDate: Date;

  gte = 0
  lte = 0

  timeValueGte = new FormControl(moment().subtract(1, 'hour').format('HH:mm'))
  timeValueLte = new FormControl(moment().format('HH:mm'))

  range = new FormGroup({
    start: new FormControl(moment()),
    end: new FormControl(moment())
  });
  siloName = new FormControl('', [Validators.required, Validators.nullValidator]);

  silosHabilitados

  data

  graph = false

  timeChart
  rangeDim
  rangeGroup

  configs = {
    configTemperature: {
      id: '#chartDiv1',
      title: 'Temperatura °C',
      columns: [
        { id: 'temp_ambiente', name: 'Temperatura Ambiente', color: 'rgb(0, 153, 77)' },
        { id: 'max_temp', name: 'Maior Temperatura do Silo', color: 'rgb(0, 102, 255)' },
        { id: 'max_temp_motor', name: 'Temperatura de Parada', color: 'rgb(255, 51, 51)' }
      ]
    },
    configHumidity: {
      id: '#chartDiv2',
      title: 'Umidade %',
      columns: [
        { id: 'max_umi', name: 'Umidade Máxima Ajustada', color: 'rgb(255, 51, 51)' },
        { id: 'med_umidade', name: 'Umidade Média Intergranular', color: 'rgb(13, 13, 13)' },
        { id: 'umidade', name: 'Umidade Ambiente', color: 'rgb(191, 191, 191)' }
      ]
    },
    configPotency: {
      id: '#chartDiv3',
      title: 'Potência Aplicada %',
      columns: [
        { id: 'potencia_motor', name: 'Potência do Motor', color: 'rgb(153, 0, 204)' },
      ]
    }
  }

  constructor(
    private loadingService: LoadingService,
    private siloService: SiloService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.loadingService.startingLoading()
    this.siloService.get_silos().subscribe({
      next: v => {
        if (v.length > 0) {
          this.silosHabilitados = v
        } else {
          alert('Nenhum silo cadastrado')
          this.loadingService.endingLoading()
          this.router.navigate(['/ks/settings']);
        }
      },
      complete: () => {
        this.siloName.setValue(this.silosHabilitados[0])
        this.loadingService.endingLoading()
      },
      error: e => {
        this.loadingService.endingLoading()
        console.log('error: ', e)
      },
    })

    this.timeChange('lte')
    this.timeChange('gte')
  }

  timeChange(type) {
    if (type == 'gte') {
      let hour = this.timeValueGte.value.split(':')[0]
      let min = this.timeValueGte.value.split(':')[1]
      let aux = moment(new Date(this.range.value.start).toLocaleString('pt-br').split(' ')[0], 'D/M/YYYY')
      this.gte = aux.add(hour, 'hours').add(min, 'minutes').valueOf()
    } else if (type == 'lte') {
      let hour = this.timeValueLte.value.split(':')[0]
      let min = this.timeValueLte.value.split(':')[1]
      let aux = moment(new Date(this.range.value.end).toLocaleString('pt-br').split(' ')[0], 'D/M/YYYY')
      this.lte = aux.add(hour, 'hours').add(min, 'minutes').valueOf()
    }
  }

  getSiloData() {
    this.loadingService.startingLoading()
    this.graph = false
    let error = false
    this.siloService.get_general_data(this.siloName.value['silo'], this.gte, this.lte).subscribe({
      next: v => {
        if (v.length != 0) {
          this.data = v
          this.data.map(m => {
            m.time = new Date(m.time)
          })
        } else {
          alert('Sem dados')
          this.loadingService.endingLoading()
          error = true
        }

      },
      complete: () => {
        if (error == false) {
          this.graph = true
          setTimeout(() => {
            this.loadSharedConfigs()
          }, 100);
        }
      }
    })
  }

  render() {
    dc.renderAll(this.group_name)
    window.onresize = () => dc.renderAll(this.group_name)
    this.loadingService.endingLoading();
  }

  loadSharedConfigs() {
    let xf = crossfilter(this.data);
    this.rangeDim = xf.dimension(d => d.time)
    this.rangeGroup = this.rangeDim.group().reduce(
      (p, v) => {
        p.value += v.value;
        return p;
      },
      (p, v) => {
        p.value -= v.value;
        return p;
      },
      () => { return { value: 0 }; });

    this.minDate = this.rangeDim.bottom(1)[0].time
    this.maxDate = this.rangeDim.top(1)[0].time

    let timeChart = {}

    for (let i of ['#chartDivTime1', '#chartDivTime2', '#chartDivTime3']) {
      timeChart[i] = dc.lineChart(i, this.group_name)
        .dimension(this.rangeDim)
        .height(100)
        .margins({ top: 0, right: 40, bottom: 0, left: 60 })
        .x(d3.scaleTime().domain([this.minDate, this.maxDate]))
        .group(this.rangeGroup)
        .renderDataPoints({ radius: 3, fillOpacity: 1, strokeOpacity: 1 })
        .valueAccessor(p => 100)
        .colors('#BA68C8')
        .on('pretransition', function (chart) {
          d3.select(`${i} > svg > g > g.axis.x`)
            .style('display', 'none')
          d3.select(`${i} > svg > g > g.axis.y`)
            .style('display', 'none')
          d3.select(`${i} > svg > g > g.chart-body`)
            .style('display', 'none')
        })
    }

    this.loadLineChart(this.configs['configTemperature'], timeChart['#chartDivTime1'])
    this.loadLineChart(this.configs['configHumidity'], timeChart['#chartDivTime2'])
    this.loadLineChart(this.configs['configPotency'], timeChart['#chartDivTime3'])

    this.render()
  }

  loadLineChart(config, timeChart) {
    config['obj'] = dc.compositeChart(config['id'], this.group_name);

    var dcCharts = []

    for (let column of config['columns']) {
      let sensorGroup = this.rangeDim.group().reduceSum(dc.pluck(column['id']));
      let lineChart = dc.lineChart(config['obj']).curve(d3.curveMonotoneX)
        .group(sensorGroup, `${column['name']}`)
        .renderDataPoints({ radius: 2, fillOpacity: 1, strokeOpacity: 1 })
        .dimension(this.rangeDim)
        // .renderArea(false)
        // .valueAccessor(d => {
        //   return d.value
        // })
        .colors(column['color'])
      dcCharts.push(lineChart)
    }

    config['obj']
      .margins({ top: 100, right: 40, bottom: 40, left: 45 })
      .rangeChart(timeChart)
      .brushOn(false)
      .height(300)
      .x(d3.scaleTime().domain([this.minDate, this.maxDate]))
      .mouseZoomable(false)
      .renderHorizontalGridLines(true)
      .renderVerticalGridLines(true)
      .yAxisLabel(config['title'], 20)
      // .legend(dc.legend().x(40).y(20).horizontal(true).autoItemWidth(true).gap(10).legendWidth('100%').highlightSelected(true))
      .legend(dc.legend().x(100).y(30))
      .shareTitle(false)
      // .round(d3.timeMonth.round)
      .elasticY(true)
      .clipPadding([10])
      .compose(dcCharts)
    // .on('pretransition', function (chart) {
    //   d3.selectAll('.line')
    //     .style('fill', 'none')
    //   d3.select('.text')
    //     .style('fill', 'none')
    // })

    return true
  }
}
