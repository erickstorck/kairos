import { Component, OnInit } from '@angular/core';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { SiloService } from '../../silos/silo.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as d3 from 'd3';
import * as dc from 'dc';
import * as crossfilter from 'crossfilter2/crossfilter';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-sensors',
  templateUrl: './sensors.component.html',
  styleUrls: ['./sensors.component.css']
})
export class SensorsComponent implements OnInit {

  group_name: string = "chartContainer";
  minDate: Date;
  maxDate: Date;

  gte = 0
  lte = 0

  timeValueGte = new FormControl(moment().subtract(1, 'hour').format('HH:mm'))
  timeValueLte = new FormControl(moment().format('HH:mm'))

  range = new FormGroup({
    start: new FormControl(moment()),
    end: new FormControl(moment())
  });
  siloName = new FormControl('', [Validators.required, Validators.nullValidator]);
  sensor = new FormControl('', [Validators.required, Validators.nullValidator]);
  pendulo = new FormControl('', [Validators.required, Validators.nullValidator]);

  silosHabilitados
  pendulos = []
  sensores = []

  siloData
  filteredData

  graph = false

  constructor(
    private loadingService: LoadingService,
    private siloService: SiloService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.loadingService.startingLoading()
    this.siloService.get_silos().subscribe({
      next: v => {
        if (v.length > 0) {
          this.silosHabilitados = v
        } else {
          alert('Nenhum silo cadastrado')
          this.loadingService.endingLoading()
          this.router.navigate(['/ks/settings']);
        }
      },
      complete: () => {
        this.siloName.setValue(this.silosHabilitados[0])
        this.loadingService.endingLoading()
      },
      error: e => {
        this.loadingService.endingLoading()
        console.log('error: ', e)
      },
    })

    this.timeChange('lte')
    this.timeChange('gte')
  }

  timeChange(type) {
    if (type == 'gte') {
      let hour = this.timeValueGte.value.split(':')[0]
      let min = this.timeValueGte.value.split(':')[1]
      let aux = moment(new Date(this.range.value.start).toLocaleString('pt-br').split(' ')[0], 'D/M/YYYY')
      this.gte = aux.add(hour, 'hours').add(min, 'minutes').valueOf()
    } else if (type == 'lte') {
      let hour = this.timeValueLte.value.split(':')[0]
      let min = this.timeValueLte.value.split(':')[1]
      let aux = moment(new Date(this.range.value.end).toLocaleString('pt-br').split(' ')[0], 'D/M/YYYY')
      this.lte = aux.add(hour, 'hours').add(min, 'minutes').valueOf()
    }
  }

  filterPendulo() {
    this.sensores = []
    this.filteredData = this.siloData[this.pendulo.value]
    let lastData = this.filteredData[Object.keys(this.filteredData)[Object.keys(this.filteredData).length - 1]]
    for (let key of Object.keys(lastData)) {
      if (lastData[key] == "True") this.sensores.push(key)
    }
    this.sensor = new FormControl(this.sensores, [Validators.required, Validators.nullValidator]);
    // this.graph = true
  }

  getSiloData() {
    this.loadingService.startingLoading()
    this.graph = false
    this.sensores = []
    this.pendulo.setValue(null)

    this.siloService.get_sensor_data(this.siloName.value['silo'], this.gte, this.lte).subscribe({
      next: v => {
        if (v.hasOwnProperty('pendulo_01') && v['pendulo_01'].length > 0) {
          this.siloData = v
          this.pendulos = Object.keys(this.siloData)
        } else {
          alert('Sem dados')
          this.pendulos = []
          this.loadingService.endingLoading()
        }
      },
      complete: () => this.loadingService.endingLoading()
    })
  }

  send() {
    this.graph = true
    setTimeout(() => {
      this.render(this.filteredData)
    }, 100);
  }

  render(data) {
    data.map(m => {
      m.time = new Date(m.time)
    })

    let xf = crossfilter(data);
    if (this.loadLineChart(xf)) {
      dc.renderAll(this.group_name)
      window.onresize = () => dc.renderAll(this.group_name)
      this.loadingService.endingLoading();
    }

  }

  loadLineChart(xf) {
    let rangeDim = xf.dimension(d => d.time)

    let rangeGroup = rangeDim.group().reduce(
      (p, v) => {
        p.value += v.value;
        return p;
      },
      (p, v) => {
        p.value -= v.value;
        return p;
      },
      () => { return { value: 0 }; });

    this.minDate = rangeDim.bottom(1)[0].time
    this.maxDate = rangeDim.top(1)[0].time

    const mainLine = dc.compositeChart("#charDiv1", this.group_name);

    var dcCharts = []

    for (let sensor of this.sensor.value) {
      let sensorGroup = rangeDim.group().reduceSum(dc.pluck(`temp_${sensor.slice(-2)}`));
      let totalSensors = this.sensor.value.length
      let numberSensor = this.sensor.value.indexOf(sensor) + 1
      let lineChart = dc.lineChart(mainLine).curve(d3.curveCardinal.tension(0.5))
        .group(sensorGroup, `Sensor ${sensor.slice(-2)}`)
        .renderDataPoints({ radius: 3, fillOpacity: 1, strokeOpacity: 1 })
        .dimension(rangeDim)
        .renderArea(false)
        .colors(d3.interpolateTurbo(numberSensor / totalSensors))
      dcCharts.push(lineChart)
    }

    const timeChart = dc.lineChart("#charDiv2", this.group_name)
      .dimension(rangeDim)
      .height(100)
      .margins({ top: 0, right: 40, bottom: 0, left: 60 })
      .x(d3.scaleTime().domain([this.minDate, this.maxDate]))
      .group(rangeGroup)
      .renderDataPoints({ radius: 3, fillOpacity: 1, strokeOpacity: 1 })
      .valueAccessor(p => 100)
      .colors('#BA68C8')
      .on('pretransition', function (chart) {
        d3.select('#charDiv2 > svg > g > g.axis.x')
          .style('display', 'none')
        d3.select('#charDiv2 > svg > g > g.axis.y')
          .style('display', 'none')
        d3.select('#charDiv2 > svg > g > g.chart-body')
          .style('display', 'none')
      })

    mainLine
      .margins({ top: 100, right: 40, bottom: 40, left: 45 })
      .rangeChart(timeChart)
      .brushOn(false)
      .height(500)
      .x(d3.scaleTime().domain([this.minDate, this.maxDate]))
      .mouseZoomable(false)
      .renderHorizontalGridLines(true)
      .renderVerticalGridLines(true)
      .yAxisLabel("Temperatura °C", 20)
      // .legend(dc.legend().x(40).y(20).horizontal(true).autoItemWidth(true).gap(10).legendWidth('100%').highlightSelected(true))
      .legend(dc.legend().x(10).y(10).itemHeight(13).gap(10).autoItemWidth(true).horizontal(true).maxItems(25))
      // .legend(dc.legend().x(100).y(30))
      .shareTitle(false)
      .round(d3.timeMonth.round)
      .elasticY(true)
      .clipPadding([10])
      .compose(dcCharts)
      .on('pretransition', function (chart) {
        d3.selectAll('.line')
          .style('fill', 'none')
        d3.select('.text')
          .style('fill', 'none')
      })

    return true
  }

}
