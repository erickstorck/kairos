import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ExportToCsv } from 'export-to-csv';
import { jsPDF } from "jspdf";
import autoTable from 'jspdf-autotable'
import * as moment from 'moment';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { SettingsService } from '../settings/settings.service';
import { SiloService } from '../silos/silo.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  range = new FormGroup({
    start: new FormControl(moment()),
    end: new FormControl(moment())
  });

  siloName = new FormControl('', [Validators.required, Validators.nullValidator]);
  dataType = new FormControl('', [Validators.required, Validators.nullValidator]);
  timeValueGte = new FormControl(moment().subtract(1, 'hour').format('HH:mm'))
  timeValueLte = new FormControl(moment().format('HH:mm'))

  gte = 0
  lte = 0

  silosHabilitados

  datasource = { 'Geral': [], 'Sensores': [] }

  localization = ''

  alarmObject = {
    'flag_falha_bateria': 'Bateria Anormal',
    'flag_alarme_temp': 'Alta Temperatura no Silo',
    'flag_falha_sensores': 'Falha Leitura Temperatura',
    'flag_falha_estacao_met': 'Falha Estação Meteorológica',
    'flag_falha_tx_rx': 'Falha de TX-RX',
    'flag_falha_comunicacao': 'Falha de Comunicação'
  }

  warningObject = {
    'flag_hora_ponta': 'Horário de Ponta',
    'flag_umidade_alta': 'Umidade Ambiente Alta',
    'flag_chovendo': 'Chovendo',
    'flag_ambiente_quente': 'Ambiente quente',
    'flag_motor_ligado': 'Motor Aeração ligado',
    'flag_temperatura_ok': 'Temperatura Ok',
    'flag_relogio_reajustado': 'Relógio Reajustado',
    'flag_retorno_energia': 'Retorno da Energia'
  }

  checkAll = true
  checkboxes = {
    'Geral': [
      // {
      //   col: 'local',
      //   name: 'Local',
      //   enabled: true
      // },
      // {
      //   col: 'activeFlags',
      //   name: 'Flags Ativas',
      //   enabled: true
      // },
      {
        col: 'alarms',
        name: 'Alarmes',
        enabled: true
      },
      {
        col: 'warning',
        name: 'Avisos',
        enabled: true
      },
      {
        col: 'flag_sensor_chuva',
        name: 'Sensor Chuva',
        enabled: true
      },
      {
        col: 'flag_tipo_controle',
        name: 'Tipo Controle',
        enabled: true
      },
      // {
      //   col: 'freq',
      //   name: 'Canal de operação',
      //   enabled: true
      // },
      {
        col: 'max_temp',
        name: 'Maior Temp. Silo',
        enabled: true
      },
      {
        col: 'max_temp_motor',
        name: 'Temp. Parada Motor',
        enabled: true
      },
      {
        col: 'max_umi',
        name: 'Umi. Máxima Ajustada',
        enabled: true
      },
      {
        col: 'med_umidade',
        name: 'Umi. Média Silo',
        enabled: true
      },
      {
        col: 'mode',
        name: 'Modo Operacional',
        enabled: true
      },
      // {
      //   col: 'modelo',
      //   name: 'Modelo do Silo',
      //   enabled: true
      // },
      {
        col: 'potencia_motor',
        name: 'Potência Aplicada',
        enabled: true
      },
      {
        col: 'produto',
        name: 'Produto',
        enabled: true
      },
      {
        col: 'qtd_pen',
        name: 'Quantidade Pêndulos',
        enabled: true
      },
      // {
      //   col: 'tamanho',
      //   name: 'Tamanho do Silo',
      //   enabled: true
      // },
      {
        col: 'temp_ambiente',
        name: 'Temp. Ambiente',
        enabled: true
      },
      // {
      //   col: 'tensao_bat',
      //   name: 'Tensão da Bateria',
      //   enabled: true
      // },
      {
        col: 'time',
        name: 'Data',
        enabled: true
      },
      {
        col: 'umidade',
        name: 'Umi. Ambiente',
        enabled: true
      },
    ],
    'Sensores': [
      {
        col: 'sensor_01',
        name: 'Sensor 01',
        enabled: true
      },
      {
        col: 'sensor_02',
        name: 'Sensor 02',
        enabled: true
      },
      {
        col: 'sensor_03',
        name: 'Sensor 03',
        enabled: true
      },
      {
        col: 'sensor_04',
        name: 'Sensor 04',
        enabled: true
      },
      {
        col: 'sensor_05',
        name: 'Sensor 05',
        enabled: true
      },
      {
        col: 'sensor_06',
        name: 'Sensor 06',
        enabled: true
      },
      {
        col: 'sensor_07',
        name: 'Sensor 07',
        enabled: true
      },
      {
        col: 'sensor_08',
        name: 'Sensor 08',
        enabled: true
      },
      {
        col: 'sensor_09',
        name: 'Sensor 09',
        enabled: true
      },
      {
        col: 'sensor_10',
        name: 'Sensor 10',
        enabled: true
      },
      {
        col: 'sensor_11',
        name: 'Sensor 11',
        enabled: true
      },
      {
        col: 'sensor_12',
        name: 'Sensor 12',
        enabled: true
      },
      {
        col: 'sensor_13',
        name: 'Sensor 13',
        enabled: true
      },
      {
        col: 'sensor_14',
        name: 'Sensor 14',
        enabled: true
      },
      {
        col: 'sensor_15',
        name: 'Sensor 15',
        enabled: true
      },
      {
        col: 'sensor_16',
        name: 'Sensor 16',
        enabled: true
      },
      {
        col: 'time',
        name: 'Data',
        enabled: true
      },
      // {
      //   col: 'pendulo',
      //   name: 'Pêndulo',
      //   enabled: true
      // }
    ]
  }

  dataTypes = ['Geral', 'Sensores']
  mode = { 1: 'Automático', 2: 'Manual Lig.', 3: 'Manual Desl.' }
  product = { 0: 'Grãos', 1: 'Soja', 2: 'Feijão', 3: 'Milho', 4: 'Pipoca', 5: 'Arroz', 6: 'Trigo', 7: 'Sorgo', 8: 'Lentilha', 9: 'Outros' }
  downloadData = {}
  pendulos
  csvExporter: ExportToCsv;
  options

  constructor(
    private router: Router,
    private siloService: SiloService,
    private settingsService: SettingsService,
    private loading: LoadingService
  ) { }

  ngOnInit(): void {
    this.loading.startingLoading()
    this.checkboxes['Geral'].sort((a, b) => a.name > b.name ? 1 : a.name < b.name ? -1 : 0);
    this.checkboxes['Sensores'].sort((a, b) => a.name > b.name ? 1 : a.name < b.name ? -1 : 0);
    this.siloService.get_silos().subscribe({
      next: v => {
        if (v.length > 0) {
          this.silosHabilitados = v
        } else {
          alert('Nenhum silo cadastrado')
          this.loading.endingLoading()
          this.router.navigate(['/ks/settings']);
        }
      },
      complete: () => {
        this.siloName.setValue(this.silosHabilitados[0])
        this.dataType.setValue('Geral')
        this.loading.endingLoading()
        this.range.setValue({ start: new Date(), end: new Date() })
      },
      error: e => {
        this.loading.endingLoading()
        console.log('error: ', e)
      }
    })
    this.settingsService.get_localization().subscribe({
      next: v => this.localization = v,
      error: e => console.log('error: ', e)
    })

    this.timeChange('lte')
    this.timeChange('gte')

    if (localStorage.getItem('report-checkboxes') != undefined) this.checkboxes = JSON.parse(localStorage.getItem('report-checkboxes'))
  }

  timeChange(type) {
    if (type == 'gte') {
      let hour = this.timeValueGte.value.split(':')[0]
      let min = this.timeValueGte.value.split(':')[1]
      let aux = moment(new Date(this.range.value.start).toLocaleString('pt-br').split(' ')[0], 'D/M/YYYY')
      this.gte = aux.add(hour, 'hours').add(min, 'minutes').valueOf()
    } else if (type == 'lte') {
      let hour = this.timeValueLte.value.split(':')[0]
      let min = this.timeValueLte.value.split(':')[1]
      let aux = moment(new Date(this.range.value.end).toLocaleString('pt-br').split(' ')[0], 'D/M/YYYY')
      this.lte = aux.add(hour, 'hours').add(min, 'minutes').valueOf()
    }
  }

  parseGeralData() {
    this.datasource['Geral'].map(m => {
      m['time'] = moment(m['time']).locale('pt-br').format('L') + '-' + moment(m['time']).locale('pt-br').format('LT')
      m['flag_tipo_controle'] = m['flag_tipo_controle'] == 'True' ? 'Fluxo de ar' : 'Por produto'
      m['max_temp'] = m['max_temp'] != null ? m['max_temp'].toString().replace('.', ',') + '°C' : ''
      m['max_temp_motor'] = m['max_temp_motor'] != null ? m['max_temp_motor'].toString().replace('.', ',') + '°C' : ''
      m['med_umidade'] = m['med_umidade'] != null ? m['med_umidade'] + '%' : ''
      m['temp_ambiente'] = m['temp_ambiente'] != null ? m['temp_ambiente'].toString().replace('.', ',') + '°C' : ''
      m['umidade'] = m['umidade'] != null ? m['umidade'] + '%' : ''
      m['potencia_motor'] = m['potencia_motor'] != null ? m['potencia_motor'] + '%' : ''
      m['alarm_max_temp'] = m['alarm_max_temp'] != null ? m['alarm_max_temp'].toString().replace('.', ',') + '°C' : ''
      m['max_umi'] = m['max_umi'] != null ? m['max_umi'] + '%' : ''
      m['flag_sensor_chuva'] = m['flag_sensor_chuva'] == 'True' ? 'Sim' : 'Não'
      m['mode'] = this.mode[m['mode']]
      m['produto'] = this.product[m['produto']]
      m['tensao_bat'] = ['tensao_bat'] != null ? m['tensao_bat'] + 'V' : ''
      if (Object.keys(m).includes('error')) delete m['error']
      m['alarms'] = m['activeFlags'] != null ? m['activeFlags'].replace(/\[|'|\]| /g, '').split(',').reduce((prev, curr) => {
        if (Object.keys(this.alarmObject).includes(curr)) {
          if (prev != '') {
            prev += '/' + this.alarmObject[curr]
          } else {
            prev += this.alarmObject[curr]
          }
        }
        return prev
      }, '') : '',
        m['warning'] = m['activeFlags'] != null ? m['activeFlags'].replace(/\[|'|\]| /g, '').split(',').reduce((prev, curr) => {
          if (Object.keys(this.warningObject).includes(curr)) {
            if (prev != '') {
              prev += '/' + this.warningObject[curr]
            } else {
              prev += this.warningObject[curr]
            }
          }
          return prev
        }, '') : ''
    })
  }

  saveCheckbox() {
    localStorage.setItem('report-checkboxes', JSON.stringify(this.checkboxes))
  }

  downloadPDF() {
    const doc = new jsPDF({
      orientation: "landscape",
    })

    var img = new Image()
    img.src = 'assets/imgs/kairos.png'
    doc.addImage(img, 'png', 10, 0, 60, 30)

    let head = this.localization != '' ? ['Relatório de Operação', 'Localização Geral', 'Localização do Silo', 'Intervalo'] : ['Relatório de Operação', 'Localização do Silo', 'Intervalo']
    let body = this.localization != '' ? [this.siloName.value['silo'], this.localization, this.siloName.value['local'], `De ${moment(this.range.value.start).locale('pt-br').format('L')} ${this.timeValueGte.value} à ${moment(this.range.value.end).locale('pt-br').format('L')} ${this.timeValueLte.value}`] : [this.siloName.value['silo'], this.siloName.value['local'], `De ${moment(this.range.value.start).locale('pt-br').format('L')} ${this.timeValueGte.value} à ${moment(this.range.value.end).locale('pt-br').format('L')} ${this.timeValueLte.value}`]

    autoTable(doc, {
      startY: 30,
      headStyles: { fillColor: '#26a16f' },
      head: [head],
      body: [body],
    })
    let columns
    let details = {}

    if (this.dataType.value == 'Geral') {
      columns = this.checkboxes[this.dataType.value].reduce((prev, curr) => {
        if (curr['enabled'] == true) {
          prev = prev.concat({ header: curr['name'], dataKey: curr['col'] })
        }
        return prev
      }, [])

      var time = columns.find(f => f['dataKey'] == 'time')
      var alarmes = columns.find(f => f['dataKey'] == 'alarms')
      var avisos = columns.find(f => f['dataKey'] == 'warning')
      columns = columns.filter(f => f['dataKey'] != 'time' && f['dataKey'] != 'alarms' && f['dataKey'] != 'warning')
      columns.unshift(time)
      columns.push(alarmes)
      columns.push(avisos)
    } else if (this.dataType.value == 'Sensores') {
      columns = this.checkboxes[this.dataType.value].reduce((prev, curr) => {
        if (curr['enabled'] == true) {
          if (curr['col'].includes('sensor')) {
            prev = prev.concat({ header: curr['name'], dataKey: curr['col'].replace('sensor', 'temp') })
          } else {
            prev = prev.concat({ header: curr['name'], dataKey: curr['col'] })
          }
        }
        return prev
      }, [])
      columns.splice(1,0, { header: 'Pêndulo', dataKey: 'pendulo' })
      details['foot'] = [['* Sensor Desabilitado']]
    }


    details['styles'] = { fontSize: 7 }
    details['headStyles'] = { fillColor: '#26a16f' }
    details['footStyles'] = { fillColor: '#26a16f' }
    details['body'] = this.datasource[this.dataType.value]
    details['columns'] = columns

    autoTable(doc, details)
    doc.save('table.pdf')
  }

  clearData() {
    this.datasource['Sensores'] = []
    this.datasource['Geral'] = []
  }

  getData() {
    this.loading.startingLoading()
    this.siloService.get_general_data(this.siloName.value['silo'], this.gte, this.lte).subscribe({
      next: v => {
        if (v.length != 0) {
          this.datasource['Geral'] = v
          this.parseGeralData()
        }
      },
      complete: () => {
        this.siloService.get_sensor_data(this.siloName.value['silo'], this.gte, this.lte).subscribe({
          next: v => {
            if (v['pendulo_01'] != undefined && v['pendulo_01'].length != 0) {
              let aux = []
              this.pendulos = Object.keys(v)
              this.pendulos.forEach(e => {
                v[e].map(m => {
                  this.checkboxes['Sensores'].forEach(e => {
                    if (e['col'].includes('sensor')) {
                      if (m[e['col']] == 'ND') {
                        delete m[e['col'].replace('sensor', 'temp')]
                      } else if (m[e['col']] == 'False') {
                        m[e['col'].replace('sensor', 'temp')] += ' *'
                      } else {
                        m[e['col'].replace('sensor', 'temp')] += '°C'
                      }
                    }
                  })
                  m['pendulo'] = e
                  m['time'] = moment(m['time']).locale('pt-br').format('L') + '-' + moment(m['time']).locale('pt-br').format('LT')
                  this.checkboxes[this.dataType.value].filter(f => f['enabled'] == false).map(disabled => {
                    delete m[disabled['col']]
                    if (disabled['col'].includes('sensor')) delete m['temp_' + disabled['col'].slice(-2)]
                  })
                  aux.push(m)
                })
              })
              this.datasource['Sensores'] = aux
            } else alert('Sem dados')
          },
          complete: () => {
            this.loading.endingLoading()
          },
          error: e => {
            this.loading.endingLoading()
            console.log('error: ', e)
          }
        })
      },
      error: e => {
        this.loading.endingLoading()
        console.log('errore: ', e)
      }
    })
  }

  showCheckboxes() {
    this.checkAll = true
  }

  downloadCSV() {
    this.options = {
      filename: this.dataType.value,
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: false,
      title: this.dataType.value,
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };


    if (this.dataType.value == 'Geral') {
      let data = this.datasource[this.dataType.value].reduce((prev, curr) => {
        let aux = {}
        this.checkboxes['Geral'].forEach(e => {
          if (e['enabled'] == true) aux[e['col']] = curr[e['col']]
        })
        prev = prev.concat(aux)
        return prev
      }, [])
      this.csvExporter = new ExportToCsv(this.options);
      this.csvExporter.generateCsv(data);
    } else {
      this.csvExporter = new ExportToCsv(this.options);
      this.csvExporter.generateCsv(this.datasource[this.dataType.value]);
    }

  }

  toggleAll() {
    this.checkboxes[this.dataType.value].map(m => {
      m['enabled'] = !this.checkAll
    })
  }

}
