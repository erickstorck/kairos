import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AuthService } from '../shared/auth.service';
import { decodeToken } from 'src/app/shared/utils/token';
import { MatSidenav } from '@angular/material/sidenav';
import { MatIcon, MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";
import { element } from 'protractor';
import { SettingsService } from './settings/settings.service';

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.css']
})
export class FeaturesComponent implements OnInit {
  loading = false

  role = ''
  roleIcons = {
    'admin': 'admin_panel_settings',
    'default': 'engineering'
  }

  roleTooltip = {
    'admin': 'Usuário Administrador',
    'default': 'Usuário Padrão'
  }

  features = [
    {
      route: 'ks/home',
      name: 'Home',
      icon: 'home'
    },
    {
      route: 'ks/silos',
      name: 'Silos',
      icon: 'silo'
    },
    {
      route: 'ks/graphs/operation',
      name: 'Gráficos Operação',
      icon: 'area_chart'
    },
    {
      route: 'ks/graphs/sensors',
      name: 'Gráficos Sensores',
      icon: 'thermostat'
    },
    {
      route: 'ks/report',
      name: 'Relatório',
      icon: 'content_paste'
    }
  ]

  notifications = [];
  notificationsPreview = [];
  notificationsCount = 0;

  userName = ''

  localization = ''

  @ViewChild('sidenav') sidenav: MatSidenav;

  constructor(
    private router: Router,
    private authService: AuthService,
    private loadingService: LoadingService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private settingsService: SettingsService
  ) {
    let token = decodeToken(sessionStorage.getItem('access_token'))
    this.userName = token['name']
    this.role = token['permission']
    this.loadingService.loadingListener().subscribe(load => {
      this.loading = load
    });
    this.matIconRegistry.addSvgIcon(
      "silo",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../assets/svg/silo.svg")
    );
  }

  ngOnInit(): void {
    this.settingsService.get_localization().subscribe({
      next: v => this.localization = v,
      error: e => {
        this.loadingService.endingLoading()
        console.log('error: ', e)
      }
    })
  }

  openSideMenu(toggle = true) {
    if (toggle) this.sidenav.toggle()
    let selected = sessionStorage.getItem('selected')
    let elements = document.getElementsByClassName('selected')
    for (let index = 0; index < elements.length; index++) {
      elements[index].classList.remove('selected')
    }
    if (selected) document.getElementById(selected).classList.add('selected')
  }

  goToHeaders(route) {
    let elements = document.getElementsByClassName('selected')
    for (let index = 0; index < elements.length; index++) {
      elements[index].classList.remove('selected')
    }
    sessionStorage.removeItem('selected')
    this.router.navigate([route]);
  }

  goTo(route) {
    sessionStorage.setItem('selected', route == 'ks' ? 'ks/home' : route)
    this.openSideMenu(route == 'ks' ? false : true)
    this.router.navigate([route]);
  }

  logout() {
    this.authService.doLogout();
  }
}
