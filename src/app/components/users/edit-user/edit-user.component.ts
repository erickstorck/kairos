import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'src/app/shared/auth.service';
import { AddUserComponent } from '../add-user/add-user.component';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  roles = ['Admin', 'Default']
  email = new FormControl('', [Validators.required, Validators.email, Validators.nullValidator]);
  name = new FormControl('', [Validators.required, Validators.nullValidator]);
  role = new FormControl('', [Validators.required, Validators.nullValidator]);

  constructor(
    public dialogRef: MatDialogRef<AddUserComponent>,
    private authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
    this.email.setValue(this.data.email)
    this.email.disable()
    if (this.email.value == 'admin@kairos.com.br') this.role.disable()
    this.name.setValue(this.data.name)
    this.role.setValue(this.data.role[0].toUpperCase() + this.data.role.slice(1,))
  }

  resetPassword() {
    this.authService.resetUserPassword(this.email.value).subscribe({
      next: () => alert('Senha resetada: kairos'),
      error: e => {
        if (e.status == 406) alert(e.error)
        console.log('error: ', e)
      }
    })
  }

  editUser() {
    let user = {
      name: this.name.value,
      email: this.email.value,
      role: this.role.value.toLowerCase()
    }
    this.authService.updateUser(user).subscribe({
      next: v => alert(`Usuário atualizado.`),
      error: e => {
        if (e.status == 406) alert(e.error)
        console.log('error: ', e)
      },
      complete: () => this.dialogRef.close()
    })
  }
  getErrorMessage() {
    return `Campo Inválido`
  }
}
