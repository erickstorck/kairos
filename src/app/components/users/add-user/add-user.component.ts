import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthService } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  roles = ['Admin', 'Default']
  email = new FormControl('', [Validators.required, Validators.email, Validators.nullValidator]);
  name = new FormControl('', [Validators.required, Validators.nullValidator]);
  role = new FormControl('', [Validators.required, Validators.nullValidator]);

  constructor(
    public dialogRef: MatDialogRef<AddUserComponent>,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
  }
  addUser() {
    let user = {
      name: this.name.value,
      email: this.email.value,
      role: this.role.value.toLowerCase()
    }
    this.authService.createUser(user).subscribe({
      next: v => alert(`Usuário criado.\nSenha temporária: ${v['temp_password']}`),
      error: e => {
        if (e.status == 406) alert(e.error)
        console.log('error: ', e)
      },
      complete: () => this.dialogRef.close()
    })
  }
  getErrorMessage() {
    return `Campo Inválido`
  }
}
