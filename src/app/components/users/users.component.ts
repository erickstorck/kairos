import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/app/shared/auth.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AddUserComponent } from './add-user/add-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users = []

  constructor(
    private authService: AuthService,
    public dialog: MatDialog,
    private loading: LoadingService
  ) { }

  ngOnInit(): void {
    // this.loading.startingLoading()
    this.getUsers()
  }

  getUsers() {
    this.loading.startingLoading()
    this.authService.getUsers().subscribe({
      next: v => {
        if (v.filter(f => f['email'] == 'admin@kairos.com.br').length > 1){
          this.authService.deleteUser(v.find(f => f['email'] == 'admin@kairos.com.br')['id']).subscribe({
            error: e => console.log('error: ', e)
          })
          this.getUsers()
        }
        v.map(m => {
          m['time'] = new Date(m['time']).toLocaleString('pt-br')
        })
        this.users = v
      },
      error: e => {
        this.loading.endingLoading()
        console.log('error: ', e)
      },
      complete: () => this.loading.endingLoading()
    })
  }

  editUser(user) {
    const alertModal = this.dialog.open(EditUserComponent, {
      disableClose: true,
      data: {
        name: user.name,
        email: user.email,
        role: user.role
      },
    });

    alertModal.afterClosed().subscribe(res => {
      this.getUsers()
    })
  }

  addUser() {
    const alertModal = this.dialog.open(AddUserComponent, {
      disableClose: true,
      data: {},
    });

    alertModal.afterClosed().subscribe(res => {
      this.getUsers()
    })
  }

  deleteUser(id) {
    if (confirm('Você tem certeza?')) {
      this.authService.deleteUser(id).subscribe({
        next: v => {
          this.users = this.users.filter(f => f['id'] != id)
        },
        error: e => console.log('error: ', e)
      })
    }
  }

}
