import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SiloService } from '../silo.service';

@Component({
  selector: 'app-alarm-list',
  templateUrl: './alarm-list.component.html',
  styleUrls: ['./alarm-list.component.css']
})
export class AlarmListComponent implements OnInit {

  alarms

  constructor(
    private siloService: SiloService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AlarmListComponent>,
  ) { }

  ngOnInit(): void {
    this.siloService.get_silo(this.data['silo']).subscribe({
      next: v => {
        this.alarms = [
          { alarming: v['flag_falha_comunicacao'] == 'True', name: 'Falha de Comunicação', id: 1 },
          { alarming: v['flag_ambiente_quente'] == 'True', name: 'Ambiente Quente', id: 2 },
          { alarming: v['flag_falha_estacao_met'] == 'True', name: 'Falha Estação Meteorológica', id: 3 },
          { alarming: v['flag_umidade_alta'] == 'True', name: 'Umidade Alta', id: 4 },
          { alarming: v['flag_chovendo'] == 'True', name: 'Chovendo', id: 5 },
          { alarming: v['flag_temperatura_ok'] == 'True', name: 'Temperatura Ok', id: 6 },
          { alarming: v['flag_hora_ponta'] == 'True', name: 'Horário de Ponta', id: 7 },
          { alarming: v['flag_falha_bateria'] == 'True', name: 'Bateria Anormal', id: 8 },
          { alarming: v['flag_alarme_temp'] == 'True', name: 'Alta Temperatura no Silo', id: 9 },
          { alarming: v['flag_falha_sensores'] == 'True', name: 'Falha dos Sensores', id: 10 },
          { alarming: v['flag_falha_tx_rx'] == 'True', name: 'Falha de TX-RX', id: 11 },
          { alarming: v['flag_relogio_reajustado'] == 'True', name: 'Relogio Reajustado', id: 12 },
          { alarming: v['flag_retorno_energia'] == 'True', name: 'Retorno de Energia', id: 13 }
        ]
      },
      error: e => console.log('error: ', e)
    })
  }

}
