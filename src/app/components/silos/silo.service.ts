import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SiloService {

  constructor(
    private http: HttpClient,
    public router: Router
  ) { }

  get_silo(silo) {
    return this.http.post<any[]>(`${environment.apiUrl}/silo`, { silo: silo })
  }

  get_silos() {
    return this.http.get<any[]>(`${environment.apiUrl}/silos/config`)
  }

  get_sensor_data(silo, gte, lte) {
    return this.http.post<any[]>(`${environment.apiUrl}/sensor/data`, { silo: silo, gte: gte, lte: lte })
  }

  get_general_data(silo, gte, lte) {
    return this.http.post<any[]>(`${environment.apiUrl}/general/data`, { silo: silo, gte: gte, lte: lte })
  }

  get_clear_alarms() {
    return this.http.get<any[]>(`${environment.apiUrl}/clear_alarms`, { responseType: 'text' as 'json' })
  }

  get_check_interval() {
    return this.http.get<any[]>(`${environment.apiUrl}/check_interval`)
  }

  set_check_interval(value) {
    return this.http.post<any[]>(`${environment.apiUrl}/check_interval`, { check_interval: value }, { responseType: 'text' as 'json' })
  }

  get_alarm_emails() {
    return this.http.get<any[]>(`${environment.apiUrl}/alarm_emails`)
  }

  add_alarm_email(email) {
    return this.http.post<any[]>(`${environment.apiUrl}/add_alarm_email`, { email: email }, { responseType: 'text' as 'json' })
  }

  remove_alarm_email(email) {
    return this.http.post<any[]>(`${environment.apiUrl}/remove_alarm_email`, { email: email }, { responseType: 'text' as 'json' })
  }

  set_enabled_alarms(enabledAlarms) {
    return this.http.post<any[]>(`${environment.apiUrl}/enabled_alarms`, enabledAlarms, { responseType: 'text' as 'json' })
  }

  get_enabled_alarms() {
    return this.http.get<any[]>(`${environment.apiUrl}/enabled_alarms`)
  }

}
