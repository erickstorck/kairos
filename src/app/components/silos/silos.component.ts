import { AfterViewInit, Component, OnChanges, OnInit, SimpleChange, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SiloService } from './silo.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AlarmListComponent } from './alarm-list/alarm-list.component';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-silos',
  templateUrl: './silos.component.html',
  styleUrls: ['./silos.component.css']
})

export class SilosComponent implements OnInit, OnChanges, AfterViewInit {
  range = new FormGroup({
    start: new FormControl(moment().subtract(1, 'days')),
    end: new FormControl(moment())
  });

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  siloName = new FormControl('', [Validators.required, Validators.nullValidator]);
  silosHabilitados = []
  siloConfig

  sortBy = 'time'
  sortType = 'desc'
  pageSizeOptions: number[] = [5, 10, 25, 100]

  displayedColumns = {
    'time': 'Data',
    'pendulo': 'Pêndulo',
    'temp_01': 'S1',
    'temp_02': 'S2',
    'temp_03': 'S3',
    'temp_04': 'S4',
    'temp_05': 'S5',
    'temp_06': 'S6',
    'temp_07': 'S7',
    'temp_08': 'S8',
    'temp_09': 'S9',
    'temp_10': 'S10',
    'temp_11': 'S11',
    'temp_12': 'S12',
    'temp_13': 'S13',
    'temp_14': 'S14',
    'temp_15': 'S15',
    'temp_16': 'S16'
  };
  columns
  dataSource: MatTableDataSource<any[]>

  classes = {
    'time': 'sensor_01',
    'pendulo': 'sensor_01',
    'temp_01': 'sensor_01',
    'temp_02': 'sensor_02',
    'temp_03': 'sensor_03',
    'temp_04': 'sensor_04',
    'temp_05': 'sensor_05',
    'temp_06': 'sensor_06',
    'temp_07': 'sensor_07',
    'temp_08': 'sensor_08',
    'temp_09': 'sensor_09',
    'temp_10': 'sensor_10',
    'temp_11': 'sensor_11',
    'temp_12': 'sensor_12',
    'temp_13': 'sensor_13',
    'temp_14': 'sensor_14',
    'temp_15': 'sensor_15',
    'temp_16': 'sensor_16'
  }

  operation_mode = { 1: 'Automático', 2: 'Manual', 3: 'Desligado' }
  product = { 0: 'Grãos', 1: 'Soja', 2: 'Feijão', 3: 'Milho', 4: 'Pipoca', 5: 'Arroz', 6: 'Trigo', 7: 'Sorgo', 8: 'Lentilha', 9: 'Outros' }

  coletaOut = false

  pendulos
  tamanho

  alarmingList = []
  alarms

  weather = 'assets/imgs/sun.png'

  gte = 0
  lte = 0

  timeValueGte = new FormControl('00:00')
  timeValueLte = new FormControl('23:59')

  constructor(
    private siloService: SiloService,
    private loading: LoadingService,
    private dialog: MatDialog,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.loading.startingLoading()
    this.columns = Object.keys(this.displayedColumns)
    this.siloService.get_silos().subscribe({
      next: v => {
        if (v.length > 0) {
          this.silosHabilitados = v
        } else {
          alert('Nenhum silo cadastrado')
          this.loading.endingLoading()
          this.router.navigate(['/ks/settings']);
        }
      },
      error: e => {
        this.loading.endingLoading()
        console.log('error: ', e)
      },
      complete: () => {
        this.siloName.setValue(this.silosHabilitados[0])
        this.getSiloData()
      }
    })

    this.timeChange('lte')
    this.timeChange('gte')
  }

  timeChange(type) {
    if (type == 'gte') {
      let hour = this.timeValueGte.value.split(':')[0]
      let min = this.timeValueGte.value.split(':')[1]
      let aux = moment(new Date(this.range.value.start).toLocaleString('pt-br').split(' ')[0], 'D/M/YYYY')
      this.gte = aux.add(hour, 'hours').add(min, 'minutes').valueOf()
    } else if (type == 'lte') {
      let hour = this.timeValueLte.value.split(':')[0]
      let min = this.timeValueLte.value.split(':')[1]
      let aux = moment(new Date(this.range.value.end).toLocaleString('pt-br').split(' ')[0], 'D/M/YYYY')
      this.lte = aux.add(hour, 'hours').add(min, 'minutes').valueOf()
    }
  }

  openAlarmList() {
    const alertModal = this.dialog.open(AlarmListComponent, {
      disableClose: false,
      data: { silo: this.siloName.value['silo'] },
    });

    // alertModal.afterClosed().subscribe(res => {
    //   if (res == 'updated') this.getSilos()
    // })
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    this.dataSource = new MatTableDataSource<any>(changes.datasource.currentValue)
    this.updateSortPaginator()
  }

  ngAfterViewInit() {
    this.updateSortPaginator()
  }

  updateSortPaginator() {
    if (this.dataSource) {
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }
  }

  getSiloData() {
    this.loading.startingLoading()
    this.siloService.get_silo(this.siloName.value['silo']).subscribe({
      next: v => {
        this.tamanho = v['tamanho']
        if (v.length < 1) {
          this.siloConfig = false
        } else {
          let siloDate = new Date(v['time'])
          let now = new Date()
          v['time'] = new Date(v['time']).toLocaleString('pt-br')
          this.coletaOut = siloDate < new Date(now.setMinutes(now.getMinutes() - 5)) ? true : false
          v['produto'] = this.product[v['produto']]
          v['mode'] = this.operation_mode[v['mode']]
          v['flag_tipo_controle'] = v['flag_tipo_controle'] == 'True' ? 'Fluxo de ar' : 'Por produto'
          v['flag_sensor_chuva'] = v['flag_sensor_chuva'] == 'True' ? 'Habilitado' : 'Desabilitado'
          v['flag_motor_ligado'] = v['flag_motor_ligado'] == 'True' ? 'Ligado' : 'Desligado'
          this.siloConfig = v

          this.weather = v['flag_chovendo'] == 'True' ? 'assets/imgs/rain.png' : 'assets/imgs/sun.png'

          this.alarms = [
            { alarming: v['flag_falha_comunicacao'] == 'True', name: 'Falha de Comunicação', id: 1 },
            { alarming: v['flag_ambiente_quente'] == 'True', name: 'Ambiente Quente', id: 2 },
            { alarming: v['flag_falha_estacao_met'] == 'True', name: 'Falha Estação Meteorológica', id: 3 },
            { alarming: v['flag_umidade_alta'] == 'True', name: 'Umidade Alta', id: 4 },
            { alarming: v['flag_chovendo'] == 'True', name: 'Chovendo', id: 5 },
            { alarming: v['flag_temperatura_ok'] == 'True', name: 'Temperatura Ok', id: 6 },
            { alarming: v['flag_hora_ponta'] == 'True', name: 'Horário de Ponta', id: 7 },
            { alarming: v['flag_falha_bateria'] == 'True', name: 'Bateria Anormal', id: 8 },
            { alarming: v['flag_alarme_temp'] == 'True', name: 'Alta Temperatura no Silo', id: 9 },
            { alarming: v['flag_falha_sensores'] == 'True', name: 'Falha dos Sensores', id: 10 },
            { alarming: v['flag_falha_tx_rx'] == 'True', name: 'Falha de TX-RX', id: 11 },
            { alarming: v['flag_relogio_reajustado'] == 'True', name: 'Relogio Reajustado', id: 12 },
            { alarming: v['flag_retorno_energia'] == 'True', name: 'Retorno de Energia', id: 13 }
          ]

          this.alarmingList = this.alarms.filter(f => f['alarming'] == true)
        }
      },
      error: e => {
        this.loading.endingLoading()
        console.log('error: ', e)
      },
    })

    this.siloService.get_sensor_data(this.siloName.value['silo'], this.gte, this.lte).subscribe({
      next: v => {
        if (Object.keys(v).length != 0) {
          let aux = []
          this.pendulos = Object.keys(v)
          this.pendulos.forEach(e => {
            v[e].map(m => {
              m['pendulo'] = e.replace('_', ' ')
              m['time'] = new Date(m['time']).toLocaleString('pt-br')
              aux.push(m)
            })
          })
          this.dataSource = new MatTableDataSource<any>(aux);
        } else {
          this.dataSource = new MatTableDataSource<any>([]);
          alert('Sem dados')
        }
      },
      complete: () => {
        this.updateSortPaginator()
        this.loading.endingLoading()
      },
      error: e => {
        this.loading.endingLoading()
        console.log('error: ', e)
      },
    })
  }

  applyFilter(event) {
    this.dataSource.filter = event.target.value.trim().toLowerCase()
  }

}
