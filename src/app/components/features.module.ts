import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeaturesRoutingModule } from './features-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AuthComponent } from './auth/auth.component';
import { InitialAccessComponent } from './auth/initial-access/initial-access.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NgxLoadingModule } from 'ngx-loading';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { SettingsComponent } from './settings/settings.component';
import { AddUserComponent } from './users/add-user/add-user.component';
import { EditUserComponent } from './users/edit-user/edit-user.component';
import { SilosComponent } from './silos/silos.component';
import { ReportComponent } from './report/report.component';
import { AddSiloComponent } from './settings/add-silo/add-silo.component';
import { EditSiloComponent } from './settings/edit-silo/edit-silo.component';
import { AlarmListComponent } from './silos/alarm-list/alarm-list.component';
import { SensorsComponent } from './graphs/sensors/sensors.component';
import { OperationComponent } from './graphs/operation/operation.component';
import { AlarmsComponent } from './alarms/alarms.component';
import { BackupSettingsComponent } from './backup-settings/backup-settings.component';

@NgModule({
  declarations: [
    AuthComponent,
    InitialAccessComponent,
    NotFoundComponent,
    HomeComponent,
    UsersComponent,
    SettingsComponent,
    AddUserComponent,
    EditUserComponent,
    SilosComponent,
    ReportComponent,
    AddSiloComponent,
    EditSiloComponent,
    AlarmListComponent,
    SensorsComponent,
    OperationComponent,
    AlarmsComponent,
    BackupSettingsComponent,
  ],
  imports: [
    CommonModule,
    FeaturesRoutingModule,
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ]
})
export class FeaturesModule { }
