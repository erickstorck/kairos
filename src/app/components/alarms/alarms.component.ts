import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SiloService } from '../silos/silo.service';

@Component({
  selector: 'app-alarms',
  templateUrl: './alarms.component.html',
  styleUrls: ['./alarms.component.css']
})
export class AlarmsComponent implements OnInit {

  alarms
  emails = []

  alarmObject = [
    { id: 'flag_falha_bateria', name: 'Bateria Anormal' },
    { id: 'flag_alarme_temp', name: 'Alta Temperatura no Silo' },
    { id: 'flag_falha_sensores', name: 'Falha Leitura Temperatura' },
    { id: 'flag_falha_estacao_met', name: 'Falha Estação Meteorológica' },
    { id: 'flag_falha_comunicacao', name: 'Falha de Comunicação' },
    { id: 'flag_falha_tx_rx', name: 'Falha de TX-RX' },
  ]
  warningObject = [
    { id: 'flag_hora_ponta', name: 'Horário de Ponta' },
    { id: 'flag_umidade_alta', name: 'Umidade Ambiente Alta' },
    { id: 'flag_chovendo', name: 'Chovendo' },
    { id: 'flag_ambiente_quente', name: 'Ambiente quente' },
    { id: 'flag_motor_ligado', name: 'Motor Aeração ligado' },
    { id: 'flag_temperatura_ok', name: 'Temperatura Ok' },
    { id: 'flag_relogio_reajustado', name: 'Relógio Reajustado' },
    { id: 'flag_retorno_energia', name: 'Retorno da Energia' }
  ]


  verify_time = new FormControl('1', [Validators.required, Validators.nullValidator]);
  email = new FormControl('', [Validators.required, Validators.email, Validators.nullValidator]);

  enabledFlags = new FormGroup({
    flag_falha_bateria: new FormControl(false),
    flag_alarme_temp: new FormControl(false),
    flag_falha_sensores: new FormControl(false),
    flag_falha_tx_rx: new FormControl(false),
    flag_falha_estacao_met: new FormControl(false),
    flag_falha_comunicacao: new FormControl(false),
    flag_hora_ponta: new FormControl(false),
    flag_umidade_alta: new FormControl(false),
    flag_chovendo: new FormControl(false),
    flag_ambiente_quente: new FormControl(false),
    flag_motor_ligado: new FormControl(false),
    flag_temperatura_ok: new FormControl(false),
    flag_relogio_reajustado: new FormControl(false),
    flag_retorno_energia: new FormControl(false)
  });

  constructor(
    private siloService: SiloService,
  ) { }

  ngOnInit(): void {
    this.siloService.get_check_interval().subscribe({
      next: v => this.verify_time.setValue(v['check_interval']),
      error: e => console.log('error: ', e)
    })

    this.siloService.get_enabled_alarms().subscribe({
      next: v => v.forEach(e => {
        this.enabledFlags.controls[e].setValue(true)
      })
    })

    this.updateEmailList()
  }

  updateEmailList() {
    this.siloService.get_alarm_emails().subscribe({
      next: v => this.emails = v,
      error: e => console.log('error: ', e)
    })
  }

  clearAlarms() {
    if (confirm('Resetar alarmes?')) {
      this.siloService.get_clear_alarms().subscribe({
        error: e => console.log('error: ', e),
        complete: () => {
          this.enabledFlags.controls.flag_falha_bateria.setValue(false)
          this.enabledFlags.controls.flag_alarme_temp.setValue(false)
          this.enabledFlags.controls.flag_falha_sensores.setValue(false)
          this.enabledFlags.controls.flag_falha_tx_rx.setValue(false)
          this.enabledFlags.controls.flag_falha_estacao_met.setValue(false)
          this.enabledFlags.controls.flag_falha_comunicacao.setValue(false)
          this.enabledFlags.controls.flag_hora_ponta.setValue(false)
          this.enabledFlags.controls.flag_umidade_alta.setValue(false)
          this.enabledFlags.controls.flag_chovendo.setValue(false)
          this.enabledFlags.controls.flag_ambiente_quente.setValue(false)
          this.enabledFlags.controls.flag_motor_ligado.setValue(false)
          this.enabledFlags.controls.flag_temperatura_ok.setValue(false)
          this.enabledFlags.controls.flag_relogio_reajustado.setValue(false)
          this.enabledFlags.controls.flag_retorno_energia.setValue(false)
          alert('Alarmes resetados')
        }
      })
    }
  }

  addEmail() {
    if (this.email.valid == true) {
      this.siloService.add_alarm_email(this.email.value).subscribe({
        complete: () => this.updateEmailList(),
        error: e => console.log('error: ', e)
      })
      this.email.setValue('')
    } else {
      alert('Email inválido')
    }
  }

  removeEmail(email) {
    // let email = event.target.innerText.split('\n')[0]
    if (confirm('Remover email da lista de alarmes?')) {
      this.siloService.remove_alarm_email(email).subscribe({
        complete: () => this.updateEmailList(),
        error: e => console.log('error: ', e)
      })
    }
  }

  save() {
    let aux = { 'enabled_alarms': [] }
    this.alarmObject.map(m => {
      if (this.enabledFlags.value[m['id']] == true) aux['enabled_alarms'].push(m['id'])
    })
    this.warningObject.map(m => {
      if (this.enabledFlags.value[m['id']] == true) aux['enabled_alarms'].push(m['id'])
    })

    this.siloService.set_enabled_alarms(aux).subscribe({
      error: e => console.log('error: ', e),
      complete: () => {
        this.siloService.set_check_interval(this.verify_time.value).subscribe({
          error: e => console.log('error: ', e),
          complete: () => {
            alert('Salvo')
            this.clearAlarms()
          }
        })
      }
    })
  }

}
