import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FeaturesComponent } from './features.component';
import { AuthComponent } from './auth/auth.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AuthGuard } from '../shared/auth.guard';
import { TokenGuard } from '../shared/token.guard';
import { PermissionGuard } from '../shared/permission.guard';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { SettingsComponent } from './settings/settings.component';
import { SilosComponent } from './silos/silos.component';
import { ReportComponent } from './report/report.component';
import { SensorsComponent } from './graphs/sensors/sensors.component';
import { OperationComponent } from './graphs/operation/operation.component';
import { AlarmsComponent } from './alarms/alarms.component';
import { BackupSettingsComponent } from './backup-settings/backup-settings.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', canActivate: [AuthGuard], component: AuthComponent },
  {
    path: 'ks', component: FeaturesComponent, children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', canActivate: [TokenGuard, PermissionGuard], component: HomeComponent, data: { role: 'default' } },
      { path: 'graphs/sensors', canActivate: [TokenGuard, PermissionGuard], component: SensorsComponent, data: { role: 'default' } },
      { path: 'graphs/operation', canActivate: [TokenGuard, PermissionGuard], component: OperationComponent, data: { role: 'default' } },
      { path: 'silos', canActivate: [TokenGuard, PermissionGuard], component: SilosComponent, data: { role: 'default' } },
      { path: 'report', canActivate: [TokenGuard, PermissionGuard], component: ReportComponent, data: { role: 'default' } },
      { path: 'alarms', canActivate: [TokenGuard, PermissionGuard], component: AlarmsComponent, data: { role: 'admin' } },
      { path: 'users', canActivate: [TokenGuard, PermissionGuard], component: UsersComponent, data: { role: 'admin' } },
      { path: 'settings', canActivate: [TokenGuard, PermissionGuard], component: SettingsComponent, data: { role: 'admin' } },
      { path: 'backup/settings', canActivate: [TokenGuard, PermissionGuard], component: BackupSettingsComponent, data: { role: 'admin' } },
    ]
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class FeaturesRoutingModule { }
