import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-initial-access',
  templateUrl: './initial-access.component.html',
  styleUrls: ['./initial-access.component.css']
})
export class InitialAccessComponent implements OnInit {

  name = new FormControl(null, [Validators.nullValidator],);
  email = new FormControl({ value: this.userData.email, disabled: true }, [Validators.email, Validators.nullValidator]);
  password = new FormControl(null, [Validators.required, Validators.nullValidator, Validators.minLength(6)]);
  confirmPass = new FormControl(null, [Validators.required, Validators.nullValidator, Validators.minLength(6)]);

  hide = true;

  constructor(
    private authService: AuthService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<InitialAccessComponent>,
    @Inject(MAT_DIALOG_DATA) public userData: any,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.name.setValue(this.userData.name)
  }

  hasData() {
    return Object.entries(this.userData).length > 0;
  }

  submit() {
    const user = {
      name: this.name.value,
      email: this.userData.email,
      password: this.password.value
    };

    this.authService.updateUser(user).subscribe({
      error: e => alert(`error: ${e}`),
      complete: () => {
        this.authService.signIn(user).subscribe({
          next: v => sessionStorage.setItem('access_token', v.token),
          error: e => alert(`error: ${e}`),
          complete: () => {
            this.router.navigate(['ks'])
            this.dialogRef.close()
          }
        })
      }
    })
  }

  resetPassword() {

  }

  getNameErrorMessage() {
    if (this.name.hasError('required')) {
      return 'Insira um valor';
    }
  }

  getEmailErrorMessage() {
    if (this.email.hasError('required')) {
      return 'Insira um valor';
    }

    return this.email.hasError('email') ? 'Email inválido' : '';
  }

  getPasswordErrorMessage() {
    if (this.password.hasError('required')) {
      return 'Insira um valor';
    }

    if (this.password.value.length < 6) {
      this.password.setErrors({ 'incorrect': true });
      return 'A senha deve ter no mínimo 6 caracteres';
    } else {
      this.password.setErrors({ 'incorrect': false });
    }

    if (this.password.value === this.userData.password) {
      this.password.setErrors({ 'incorrect': true });
      return 'Senha incorreta';
    } else {
      this.password.setErrors({ 'incorrect': false });
    }
  }

  getConfirmpasswordErrorMessage() {
    if (this.confirmPass.hasError('required')) {
      return 'Insira um valor';
    }

    if (this.confirmPass.value.length < 6) {
      this.confirmPass.setErrors({ 'incorrect': true });
      return 'A senha deve ter no mínimo 6 caracteres';
    } else {
      this.confirmPass.setErrors({ 'incorrect': false });
    }

    if (this.confirmPass.value === this.userData.password) {
      this.confirmPass.setErrors({ 'incorrect': true });
      return 'A senha deve ser diferente da senha padrão';
    } else {
      this.confirmPass.setErrors({ 'incorrect': false });
    }

    if (this.password.value !== this.confirmPass.value) {
      this.confirmPass.setErrors({ 'incorrect': true });
      return 'As senhas não conferem';
    } else {
      this.confirmPass.setErrors({ 'incorrect': false });
    }
  }

  closeDialog() {
    this.dialogRef.close({ redirectToHome: false });
  }

}
