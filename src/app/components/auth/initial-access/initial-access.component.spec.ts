import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InitialAccessComponent } from './initial-access.component';

describe('InitialAccessComponent', () => {
  let component: InitialAccessComponent;
  let fixture: ComponentFixture<InitialAccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InitialAccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InitialAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
