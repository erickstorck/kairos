import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { decodeToken } from 'src/app/shared/utils/token';
import { InitialAccessComponent } from './initial-access/initial-access.component';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  email = new FormControl('', [Validators.required, Validators.email, Validators.nullValidator]);
  password = new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(6)]);
  hide = true;
  token

  constructor(
    private router: Router,
    private authService: AuthService,
    public dialog: MatDialog,
    private loading: LoadingService
  ) { }

  ngOnInit(): void {
  }

  getErrorMessage(errorType) {
    if (errorType == 'email') {
      if (this.email.hasError('required')) {
        return 'Campo necessário';
      }

      return this.email.hasError('email') ? 'Email Inválido' : '';
    } else if (errorType == 'password') {
      if (this.password.hasError('required')) {
        return 'Campo necessário';
      }
      return 'Mínimo 6 digitos'
    }
  }

  getPassErrorMessage() {
    if (this.password.hasError('required')) {
      return 'Campo necessário';
    }
  }

  submit() {
    this.loading.startingLoading()
    var user: any = {};
    user.email = this.email.value;
    user.password = this.password.value;

    this.authService.signIn(user).subscribe({
      next: v => {
        this.token = v.token
      },
      error: e => {
        alert(e.error)
      },
      complete: () => {
        this.loading.endingLoading()
        let token = decodeToken(this.token)
        if (token['dflt_pwd']) {
          const alertModal = this.dialog.open(InitialAccessComponent, {
            disableClose: true,
            data: {
              name: token['name'],
              email: user.email
            },
          });
        } else {
          sessionStorage.setItem('access_token', this.token)
          sessionStorage.setItem('selected', 'ks/home')
          this.router.navigate(['ks'])
        }
      }
    })
  }
}
