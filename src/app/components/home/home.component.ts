import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { SiloService } from '../silos/silo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  silos
  configs = []

  operation_mode = { 1: 'Automático', 2: 'Manual', 3: 'Desligado' }
  product = { 0: 'Grãos', 1: 'Soja', 2: 'Feijão', 3: 'Milho', 4: 'Pipoca', 5: 'Arroz', 6: 'Trigo', 7: 'Sorgo', 8: 'Lentilha', 9: 'Outros' }

  constructor(
    private siloService: SiloService,
    private loading: LoadingService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.loading.startingLoading()
    this.siloService.get_silos().subscribe({
      next: v => {
        this.silos = v
      },
      complete: () => {
        if (this.silos.length > 0) {
          this.silos.map(m => {
            this.siloService.get_silo(m['silo']).subscribe({
              next: v => {
                console.log(v)
                if (Object.keys(v).length > 0) {
                  v['produto'] = this.product[v['produto']]
                  v['mode'] = this.operation_mode[v['mode']]
                  v['flag_tipo_controle'] = v['flag_tipo_controle'] ? 'Fluxo de ar' : 'Por produto'
                  v['local'] = m['local']
                  this.configs.push(v)
                }
              },
              complete: () => {
                console.log(this.configs)
                this.loading.endingLoading()
              },
              error: e => {
                this.loading.endingLoading()
                console.log('error: ', e)
              }
            })
          })
        } else {
          alert('Nenhum silo cadastrado')
          this.loading.endingLoading()
          this.router.navigate(['/ks/settings']);
        }
      },
      error: e => {
        this.loading.endingLoading()
        console.log('error: ', e)
      },
    })
  }

}
