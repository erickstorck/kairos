import { Component, OnInit } from '@angular/core';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { BackupSettingsService } from './backup-settings.service';
import * as moment from 'moment';

@Component({
  selector: 'app-backup-settings',
  templateUrl: './backup-settings.component.html',
  styleUrls: ['./backup-settings.component.css']
})
export class BackupSettingsComponent implements OnInit {

  backupList = []

  constructor(
    private backupService: BackupSettingsService,
    private loading: LoadingService
  ) { }

  ngOnInit(): void {
    this.updateBackupList()
  }

  updateBackupList() {
    this.backupService.get_backup_list().subscribe({
      next: v => {
        this.backupList = v.sort().reverse().map(m => moment(m, "YYMMDD_hhmmss").format('DD/MM/YYYY HH:mm:ss'))
      },
      error: e => console.log('error: ', e)
    })
  }

  doBackup() {
    this.loading.startingLoading()
    this.backupService.backup().subscribe({
      error: e => {
        this.loading.endingLoading()
        console.log('error: ', e)
      },
      complete: () => {
        setTimeout(() => {
          this.updateBackupList()
          this.loading.endingLoading()
        }, 2000)
      }
    })
  }

  doRestore(backup) {
    if (confirm('Tem certeza?')) {
      backup = moment(backup, 'DD/MM/YYYY HH:mm:ss').format("YYMMDD_HHmmss")
      this.loading.startingLoading()
      this.backupService.restore(backup).subscribe({
        error: e => {
          this.loading.endingLoading()
          console.log('error: ', e)
        },
        complete: () => {
          setTimeout(() => {
            this.updateBackupList()
            this.loading.endingLoading()
            alert('Backup Restaurado')
          }, 2000)
        }
      })
    }
  }

  delete(backup) {
    if (confirm('Tem certeza?')) {
      backup = moment(backup, 'DD/MM/YYYY HH:mm:ss').format("YYMMDD_HHmmss")
      this.loading.startingLoading()
      this.backupService.delete_backup(backup).subscribe({
        error: e => {
          this.loading.endingLoading()
          console.log('error: ', e)
        },
        complete: () => {
          this.updateBackupList()
          this.loading.endingLoading()
        }
      })
    }
  }

  flashRestore() {
    this.loading.startingLoading()
    this.backupService.flash_restore().subscribe({
      error: e => {
        this.loading.endingLoading()
        console.log('error: ', e)
      },
      complete: () => {
        setTimeout(() => {
          this.updateBackupList()
          this.loading.endingLoading()
        }, 2000)
      }
    })
  }
}
