import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BackupSettingsService {

  constructor(
    private http: HttpClient,
    public router: Router
  ) { }

  get_backup_list() {
    return this.http.get<any[]>(`${environment.apiUrl}/backup/list`)
  }

  delete_backup(backup) {
    return this.http.delete<any[]>(`${environment.apiUrl}/backup/delete/${backup}`, { responseType: 'text' as 'json' })
  }

  backup() {
    return this.http.get<any[]>(`${environment.apiUrl}/backup`, { responseType: 'text' as 'json' })
  }

  restore(backup) {
    return this.http.post<any[]>(`${environment.apiUrl}/restore`, { "backup": backup }, { responseType: 'text' as 'json' })
  }

  flash_restore() {
    return this.http.get<any[]>(`${environment.apiUrl}/restore/flash`, { responseType: 'text' as 'json' })
  }
}
