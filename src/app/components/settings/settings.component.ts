import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AddSiloComponent } from './add-silo/add-silo.component';
import { EditSiloComponent } from './edit-silo/edit-silo.component';
import { SettingsService } from './settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {


  autoCollect = false
  silosConfig
  silos = []
  collect = new FormControl('');
  localization = new FormControl('preencher', [Validators.required, Validators.nullValidator]);

  localizationEditor = false

  constructor(
    private settingsService: SettingsService,
    public dialog: MatDialog,
    private loading: LoadingService
  ) { }

  ngOnInit(): void {
    this.settingsService.get_collect_status().subscribe({
      next: v => {
        this.autoCollect = v['autoStart']
        v['collect'] == 'start' ? this.collect.setValue(true) : this.collect.setValue(false)
      },
      error: e => console.log('error: ', e)
    })
    this.getSilos()

    this.settingsService.get_localization().subscribe({
      next: v => {
        if (v != '') {
          this.localization.setValue(v)
        }
      },
      error: e => console.log('error: ', e)
    })
  }

  getSilos() {
    this.loading.startingLoading()
    this.settingsService.get_silos().subscribe({
      next: v => {
        if (v.length == 0) this.loading.endingLoading()
        this.silosConfig = v
        this.silos = []
      },
      error: e => {
        console.log('error: ', e)
        this.loading.endingLoading()
      },
      complete: () => {
        this.silosConfig.map(m => {
          this.settingsService.get_silo(m['silo']).subscribe({
            next: v => {
              if (v.length == 0) {
                this.silos.push({
                  time: 'Indisponível',
                  tamanho: 'Indisponível',
                  qtd_pen: 'Indisponível',
                  conn: 'connOff',
                  silo: m['silo'],
                  freq: m['freq'],
                  local: m['local']
                })
              } else {
                let siloDate = new Date(v['time'])
                let now = new Date()
                siloDate < new Date(now.setMinutes(now.getMinutes() - 5)) ? v['conn'] = 'connOff' : v['conn'] = 'connOn'
                v['time'] = new Date(v['time']).toLocaleString('pt-br')
                v['freq'] = m['freq']
                v['local'] = m['local']
                this.silos.push(v)
              }
            },
            error: e => {
              this.loading.endingLoading()
              console.log('error: ', e)
            },
            complete: () => this.loading.endingLoading()
          })
        })
      }
    })
  }

  addSilo() {
    const alertModal = this.dialog.open(AddSiloComponent, {
      disableClose: true,
      data: {},
    });

    alertModal.afterClosed().subscribe(res => {
      if (res == 'created') this.getSilos()
    })
  }

  editSilo(silo) {
    const alertModal = this.dialog.open(EditSiloComponent, {
      disableClose: true,
      data: { silo: silo },
    });

    alertModal.afterClosed().subscribe(res => {
      if (res == 'updated') this.getSilos()
    })
  }

  deleteSilo(silo) {
    if (confirm('Tem certeza?')) {
      this.settingsService.delete_silo(silo).subscribe({
        error: e => console.log('error: ', e),
        complete: () => {
          alert('Silo deletado')
          this.getSilos()
        }
      })
    }
  }

  toggleCollect() {
    this.settingsService.toggle_collect(this.collect.value ? 'stop' : 'start', this.autoCollect).subscribe({
      error: e => console.log('error: ', e)
    })
  }

  changeLocalization() {
    this.localizationEditor = !this.localizationEditor
    if (this.localizationEditor == true) {
      this.localization.setValue('')
    } else {
      this.localization.setValue('preencher')
    }
  }

  saveLocalization() {
    this.localizationEditor = false
    this.settingsService.set_localization(this.localization.value).subscribe({
      error: e => console.log('error: ', e),
      complete: () => {
        alert('Salvo')
        location.reload()
      }
    })
  }

}
