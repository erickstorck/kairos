import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  headers = new HttpHeaders().set('Content-Type', 'text/plain');
  currentUser = {};
  constructor(
    private http: HttpClient,
    public router: Router
  ) {
  }

  get_silos() {
    return this.http.get<any[]>(`${environment.apiUrl}/silos/config`)
  }

  get_silo(silo) {
    return this.http.post<any[]>(`${environment.apiUrl}/silo`, { silo: silo })
  }

  delete_silo(silo) {
    return this.http.delete<any[]>(`${environment.apiUrl}/silo/${silo}`, { responseType: 'text' as 'json' })
  }

  set_silo_config(silo, freq, local) {
    return this.http.post<any[]>(`${environment.apiUrl}/silo/config`, { silo: silo, freq: freq, local: local }, { responseType: 'text' as 'json' })
  }

  toggle_collect(collect, autoStart) {
    return this.http.post<any[]>(`${environment.apiUrl}/collect`, { collect: collect, autoStart: autoStart }, { responseType: 'text' as 'json' })
  }

  get_collect_status() {
    return this.http.get<any[]>(`${environment.apiUrl}/collect`)
  }

  get_localization() {
    return this.http.get<any>(`${environment.apiUrl}/silos/localization`)
  }

  set_localization(local) {
    return this.http.post<any[]>(`${environment.apiUrl}/silos/localization`, { local: local }, { responseType: 'text' as 'json' })
  }
}
