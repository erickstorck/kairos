import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-edit-silo',
  templateUrl: './edit-silo.component.html',
  styleUrls: ['./edit-silo.component.css']
})
export class EditSiloComponent implements OnInit {

  silo = new FormControl('', [Validators.required, Validators.nullValidator]);
  freq = new FormControl('', [Validators.required, Validators.nullValidator]);
  local = new FormControl('', [Validators.required, Validators.nullValidator]);

  constructor(
    public dialogRef: MatDialogRef<EditSiloComponent>,
    private settingsService: SettingsService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
    this.silo.setValue(this.data['silo']['silo'])
    this.freq.setValue(this.data['silo']['freq'])
    this.local.setValue(this.data['silo']['local'])
    this.silo.disable()
  }

  editSilo() {
    this.settingsService.set_silo_config(this.silo.value, this.freq.value, this.local.value).subscribe({
      next: v => alert('Silo atualizado'),
      error: e => console.log('error: ', e),
      complete: () => this.dialogRef.close('updated')
    })
  }
  getErrorMessage() {
    return `Campo Inválido`
  }
}
