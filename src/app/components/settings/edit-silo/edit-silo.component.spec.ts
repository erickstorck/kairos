import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSiloComponent } from './edit-silo.component';

describe('EditSiloComponent', () => {
  let component: EditSiloComponent;
  let fixture: ComponentFixture<EditSiloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditSiloComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSiloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
