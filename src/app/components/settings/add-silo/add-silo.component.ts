import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-add-silo',
  templateUrl: './add-silo.component.html',
  styleUrls: ['./add-silo.component.css']
})
export class AddSiloComponent implements OnInit {

  silo = new FormControl('', [Validators.required, Validators.nullValidator]);
  freq = new FormControl('', [Validators.required, Validators.nullValidator]);
  local = new FormControl('', [Validators.required, Validators.nullValidator]);

  constructor(
    public dialogRef: MatDialogRef<AddSiloComponent>,
    private settingsService: SettingsService
  ) { }

  ngOnInit(): void {
  }
  addSilo() {
    this.settingsService.set_silo_config(this.silo.value, this.freq.value, this.local.value).subscribe({
      error: e => console.log('error: ', e),
      complete: () => this.dialogRef.close('created')
    })
  }
  getErrorMessage() {
    return `Campo Inválido`
  }
}
