import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSiloComponent } from './add-silo.component';

describe('AddSiloComponent', () => {
  let component: AddSiloComponent;
  let fixture: ComponentFixture<AddSiloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSiloComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSiloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
