* build for rasp
    ``` docker
    docker build -f dockerfile-dependencies -t registry.gitlab.com/erickstorck/kairos/dependencies:rasp .
    docker build -f dockerfile -t registry.gitlab.com/erickstorck/kairos:rasp .
    ```

* push
    ``` docker
    docker push registry.gitlab.com/erickstorck/kairos/dependencies:rasp
    docker push registry.gitlab.com/erickstorck/kairos:rasp
    ```

* FRONT-END RUN
    ``` docker
    docker run -d --restart=unless-stopped --net host --name kairos registry.gitlab.com/erickstorck/kairos:rasp
    ```

* DB RUN
    ```docker
    docker run -d --restart=unless-stopped -v /home/pi/kairos-api/backups:/home/backup --net host --name influx influxdb:1.8
    ```

* API RUN
    ```bash
    gunicorn application:app --bind 0.0.0.0:9000 --worker-class aiohttp.worker.GunicornWebWorker
    ```

## node js
https://github.com/nodejs/help/wiki/Installation

## yarn
https://lindevs.com/install-yarn-on-raspberry-pi/

## ng
https://angular.io/cli

## docker 
curl -sSL https://get.docker.com | sh
sudo usermod -aG docker pi

## pi configuration
sudo raspi-config

## static ip
sudo nano /etc/dhcpcd.conf
```
interface wlan0
static ip_address=0.0.0.0/24
static routers=192.168.0.1
static domain_name_servers=192.168.0.1
```

## pyenv
sudo apt-get install -y make build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev \
libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl

curl https://pyenv.run | bash

sudo nano ~/.bashrc

    export PATH="$HOME/.pyenv/bin:$PATH"
    eval "$(pyenv init --path)"
    eval "$(pyenv virtualenv-init -)"

exec "$SHELL"
pyenv install -v 3.9.5

## libffi issues
cd /usr/lib/arm-linux-gnueabihf/
sudo cp libffi.so.6 libffi.so.7

## kairos_db_run.sh
#!/bin/bash
docker run -d --restart=unless-stopped -v /home/pi/kairos-api/backups:/home/backup --net host --name influx influxdb:1.8
docker start -i influx

## kairos_front_run.sh
#!/bin/bash
docker run --net host --name kairos --env API_URL="http://192.168.0.50:9000" registry.gitlab.com/erickstorck/kairos:rasp
-d --restart=unless-stopped
docker start -i kairos

## supervisor
sudo apt install supervisor
sudo nano /etc/supervisor/conf.d/kairos_db.conf
```bash
    [program:kairos_db]
    command=./home/pi/kairos_db_run.sh
    autostart=true
    autorestart=true
    stderr_logfile=/var/log/db.err.log
    stdout_logfile=/var/log/db.out.log
```
```bash
    [program:kairos_api]
    command=/home/pi/kairos-api/.env/bin/gunicorn application:app --bind 0.0.0.0:9000 --worker-class aiohttp.worker.GunicornWebWorker
    autostart=true
    autorestart=true
    stderr_logfile=/var/log/api.err.log
    stdout_logfile=/var/log/api.out.log
    killasgroup=true
    startsecs=5
    directory=/home/pi/kairos-api
```
```bash
    [program:kairos_front]
    command=/home/pi/kairos_front_run.sh
    autostart=true
    autorestart=true
    stderr_logfile=/var/log/front.err.log
    stdout_logfile=/var/log/front.out.log
```
sudo service supervisor restart
sudo supervisorctl

cat /var/log/db.err.log